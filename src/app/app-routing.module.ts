import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './main-panel/dashboard/dashboard.component';
import { OrdersComponent } from './main-panel/orders/orders.component';
import { ProductComponent } from './main-panel/product/product.component';
import { OffersComponent } from './main-panel/offers/offers.component';
import { VCSpecialComponent } from './main-panel/vcspecial/vcspecial.component';
import { ShopComponent } from './main-panel/shop/shop.component';
import { AddBranchComponent } from './main-panel/add-branch/add-branch.component';
import { AccountsComponent } from './main-panel/accounts/accounts.component';


const routes: Routes = [
  { path: '', redirectTo: '/Dashboard', pathMatch: 'full' },
  { path: 'Dashboard', component: DashboardComponent, pathMatch:'full',  data: { breadcrumbs: 'Dashboard'} },
  { path: 'Orders', component: OrdersComponent, pathMatch:'full',  data: { breadcrumbs: 'Orders'} },
  { path: 'Product', component: ProductComponent, pathMatch:'full',  data: { breadcrumbs: 'Product'} },
  { path: 'Offers', component: OffersComponent, pathMatch:'full',  data: { breadcrumbs: 'Offers'} },
  { path: 'VCSpecial', component: VCSpecialComponent, pathMatch:'full',  data: { breadcrumbs: 'VC Special'} },
  { path: 'Shop', component: ShopComponent, pathMatch:'full',  data: { breadcrumbs: 'Shop'} },
  { path: 'AddBranch', component: AddBranchComponent, pathMatch:'full',  data: { breadcrumbs: 'AddBranch'} },
  { path: 'Account', component: AccountsComponent, pathMatch:'full',  data: { breadcrumbs: 'Account'} }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
