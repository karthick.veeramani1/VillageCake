import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.modules';

import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './users/login/login.component';
import { UsersComponent } from './users/users.component';
import { SignupComponent } from './users/signup/signup.component';
import { MainPanelComponent } from './main-panel/main-panel.component';
import {  FlexLayoutModule } from "@angular/flex-layout";
import { DashboardComponent } from './main-panel/dashboard/dashboard.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { OrdersComponent } from './main-panel/orders/orders.component';
import { NavComponent } from './main-panel/nav/nav.component';
import { ProfileComponent } from './main-panel/profile/profile.component';
import { AccountsComponent } from './main-panel/accounts/accounts.component';
import { OffersComponent } from './main-panel/offers/offers.component';
import { AddBranchComponent } from './main-panel/add-branch/add-branch.component';
import { ProductComponent } from './main-panel/product/product.component';
import { VCSpecialComponent } from './main-panel/vcspecial/vcspecial.component';
import { ShopComponent } from './main-panel/shop/shop.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import {AgmCoreModule} from '@agm/core';
import { MatDatepickerModule} from '@angular/material';
import { DatePipe } from '@angular/common';

import { MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { AddNewComponent } from './main-panel/product/add-new/add-new.component';
 
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  }, 
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

const googleMapsParams = {
  apiKey: environment.GOOGLE_MAPS_API_KEY,
  libraries: ['places'],
  language: 'en',
  region: 'IN'
 
};


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsersComponent,
    SignupComponent,
    MainPanelComponent,
    DashboardComponent,
    BreadcrumbsComponent,
    OrdersComponent,
    NavComponent,
    ProfileComponent,
    AccountsComponent,
    OffersComponent,
    AddBranchComponent,
    ProductComponent,
    VCSpecialComponent,
    ShopComponent,
    AddNewComponent
    

  ],
  imports: [
    BrowserModule,
    FlexLayoutModule, 
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    ReactiveFormsModule,
    MatGoogleMapsAutocompleteModule,
    AgmCoreModule.forRoot(googleMapsParams),
  ],
  entryComponents: [ AddNewComponent ],
  providers: [AuthService, MatDatepickerModule,DatePipe,
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS} ],
  bootstrap: [AppComponent]
})
export class AppModule { }

