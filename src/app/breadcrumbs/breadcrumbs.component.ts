import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss'],
})
export class BreadcrumbsComponent {
  breadcrumbs: Array<Object>;
  constructor(private router: Router, private route: ActivatedRoute) { }
  ngOnInit() {
    this.CheckingUrl();

    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(event => {     
        this.CheckingUrl();
      })
  }

  CheckingUrl(){

    this.breadcrumbs = [];
    let currentRoute = this.route.root,
      url = '';
    do {
      let childrenRoutes = currentRoute.children;
      currentRoute = null;
      childrenRoutes.forEach(route => {
        if (route.outlet === 'primary') {
          let routeSnapshot = route.snapshot;
          // console.log('snapshot:', routeSnapshot)
          url += '/' + routeSnapshot.url.map(segment => segment.path).join('/');
          this.breadcrumbs.push({
            label: route.snapshot.data.breadcrumbs,
            url: url
          });
          currentRoute = route;
        }
      })
    } while (currentRoute);
  }
}