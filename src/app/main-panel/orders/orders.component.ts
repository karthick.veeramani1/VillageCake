import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';
const moment = _rollupMoment || _moment;



@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'] 
})
export class OrdersComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  date = new FormControl({value:moment(),disabled: true});
  displayedColumns: string[] = ['#','Order Id','Date','Time', 'Who', 'Item', 'Quantity','Price','Status'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  constructor(private datePipe : DatePipe) { }


  ngOnInit() {

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
 
    // let dateval = this.datePipe.transform(Date.now(), 'MMMM d, y'); 
    // this.dataSource.filterPredicate = function(data, filter: string): boolean {  
    //     return data.orderId.toString().includes(filter) || data.date.toString().includes(dateval) ;
    //         };
      this.dateSelect(Date.now());   
  }
  dateSelect(date){
    let dateval = this.datePipe.transform(date, 'MMMM d, y');   
    this.dataSource.filter = dateval.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  } 

  

  search(value: string) {
    let dateval = this.datePipe.transform(Date.now(), 'MMMM d, y');   
    if(value =='')
    {
      this.dateSelect(Date.now());          
    }
    else{   
      this.dataSource.filter = value.trim().toLowerCase()
    }
   
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
   
  }


} 

export interface PeriodicElement {
  id:number,
  orderId: number;
  date:string;
  time:string;
  who: string;
  item: string;
  quantity: string;
  price:number;
  status:string
}

const ELEMENT_DATA: PeriodicElement[] = [
  {id:1,orderId:13211, date:'March 1, 2019',time:'12:00', who:'Karthick',item:'Black Choclate',quantity:'700grms',price:600,status:'approved'},
  {id:2,orderId:34311, date:'March 29, 2019',time:'12:00', who:'Sudhakar',item:'Black Choclate',quantity:'500grms',price:600,status:'waiting'},
  {id:3,orderId:42311, date:'March 14, 2019',time:'12:00', who:'Mahesh',item:'Black Choclate',quantity:'800grms',price:600,status:'waiting'},
  {id:4,orderId:29211, date:'March 14, 2019',time:'12:00', who:'Karthick',item:'Black Choclate',quantity:'700grms',price:600,status:'approved'},
  {id:5,orderId:34311, date:'March 29, 2019',time:'12:00', who:'Sudhakar',item:'Black Choclate',quantity:'500grms',price:600,status:'waiting'},
  {id:6,orderId:42311, date:'March 29, 2019',time:'11:00', who:'Mahesh',item:'Black Choclate',quantity:'800grms',price:600,status:'approved'},
  {id:7,orderId:29211, date:'March 29, 2019',time:'12:00', who:'Karthick',item:'Black Choclate',quantity:'700grms',price:600,status:'decline'},
  {id:8,orderId:34311, date:'March 15, 2019',time:'12:00', who:'Sudhakar',item:'Black Choclate',quantity:'500grms',price:600,status:'approved'},
  {id:8,orderId:42311, date:'March 15, 2019',time:'12:00', who:'Mahesh',item:'Black Choclate',quantity:'800grms',price:600,status:'approved'},
  {id:9,orderId:29211, date:'March 29, 2019',time:'12:00', who:'Karthick',item:'Black Choclate',quantity:'700grms',price:600,status:'waiting'},
  {id:10,orderId:34311, date:'March 29, 2019',time:'12:00', who:'Sudhakar',item:'Black Choclate',quantity:'500grms',price:600,status:'decline'},
  {id:11,orderId:42311, date:'March 29, 2019',time:'12:00', who:'Mahesh',item:'Black Choclate',quantity:'800grms',price:600,status:'waiting'},
  {id:12,orderId:29211, date:'March 29, 2019',time:'12:00', who:'Karthick',item:'Black Choclate',quantity:'700grms',price:600,status:'decline'},
  {id:29,orderId:34311, date:'March 29, 2019',time:'12:00', who:'Sudhakar',item:'Black Choclate',quantity:'500grms',price:600,status:'waiting'},
  {id:14,orderId:42311, date:'March 12, 2019',time:'12:00', who:'Mahesh',item:'Black Choclate',quantity:'800grms',price:600,status:'approved'},
  {id:15,orderId:13211, date:'March 13, 2019',time:'12:00', who:'Karthick',item:'Black Choclate',quantity:'700grms',price:600,status:'decline'},
  {id:16,orderId:34311, date:'March 13, 2019',time:'12:00', who:'Sudhakar',item:'Black Choclate',quantity:'500grms',price:600,status:'approved'},
  {id:17,orderId:42311, date:'March 13, 2019',time:'12:00', who:'Mahesh',item:'Black Choclate',quantity:'800grms',price:600,status:'decline'},

];