import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { AddNewComponent } from './add-new/add-new.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = ['#','Product Id','Product Name','Quantity','Price','Status'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  constructor(public dialog: MatDialog) { }


  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource.filterPredicate = function(data, filter: string): boolean {  
          return data.productId.toString().includes(filter) || data.productName.toString().includes(filter) ;
              };
  } 
  
  search(value: string) {   
      this.dataSource.filter = value;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }  
  }

  addNew(): void {
    const dialogRef = this.dialog.open(AddNewComponent, {
      width: '900px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}

  



export interface PeriodicElement {
  id:number,
  productId: number;
  productName: string;
  quantity:string; 
  price:number;
  status:string
}

const ELEMENT_DATA: PeriodicElement[] = [
  {id:1,productId:13211, productName:'Black Cake',quantity:'700grms',price:600,status:'true'},
  {id:2,productId:34311,  productName:'White Cake',quantity:'500grms',price:600,status:'false'},
  {id:3,productId:42311,  productName:'Ice Cream',quantity:'800grms',price:600,status:'true'},
  {id:4,productId:13211,  productName:'Black Cake',quantity:'700grms',price:600,status:'true'},
  {id:5,productId:34311,  productName:'White Cake',quantity:'500grms',price:600,status:'true'},
  {id:6,productId:42311,  productName:'Ice Cream',quantity:'800grms',price:600,status:'true'},
  {id:7,productId:13211,  productName:'Black Cake',quantity:'700grms',price:600,status:'false'},
  {id:8,productId:34311,  productName:'White Cake',quantity:'500grms',price:600,status:'true'},
  {id:8,productId:42311,  productName:'Ice Cream',quantity:'800grms',price:600,status:'true'},
  {id:9,productId:13211,  productName:'Black Cake',quantity:'700grms',price:600,status:'false'},
  {id:10,productId:34311,  productName:'White Cake',quantity:'500grms',price:600,status:'false'},
  {id:11,productId:42311,  productName:'Ice Cream',quantity:'800grms',price:600,status:'true'},
  {id:12,productId:13211,  productName:'Black Cake',quantity:'700grms',price:600,status:'false'},
  {id:13,productId:34311,  productName:'White Cake',quantity:'500grms',price:600,status:'false'},
  {id:14,productId:42311,  productName:'Ice Cream',quantity:'800grms',price:600,status:'true'},
  {id:15,productId:13211,  productName:'Black Cake',quantity:'700grms',price:600,status:'false'},
  {id:16,productId:34311,  productName:'White Cake',quantity:'500grms',price:600,status:'true'},
  {id:17,productId:42311,  productName:'Ice Cream',quantity:'800grms',price:600,status:'false'},

];
