import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VCSpecialComponent } from './vcspecial.component';

describe('VCSpecialComponent', () => {
  let component: VCSpecialComponent;
  let fixture: ComponentFixture<VCSpecialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VCSpecialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VCSpecialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
