import { NgModule } from '@angular/core';
import { MatButtonModule,MatFormFieldModule,MatInputModule,MatGridListModule,MatSidenavModule,MatIconModule, MatCardModule, MatTableModule,MatPaginatorModule,MatSortModule, MatDatepickerModule, MatNativeDateModule, MatSlideToggleModule,MatDialogModule, MatTabsModule,MatButtonToggleModule  } from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule } from '@angular/forms';


@NgModule({
    imports: [MatFormFieldModule, MatInputModule, MatButtonModule, FlexLayoutModule, FormsModule,MatGridListModule,MatSidenavModule,MatIconModule, MatCardModule, MatTableModule,MatPaginatorModule,MatSortModule, MatDatepickerModule,MatNativeDateModule, MatSlideToggleModule,MatDialogModule, MatTabsModule,MatButtonToggleModule ],
    exports: [MatFormFieldModule,MatInputModule, MatButtonModule, FlexLayoutModule, FormsModule, MatGridListModule,MatSidenavModule,MatIconModule, MatCardModule, MatTableModule,MatPaginatorModule,MatSortModule, MatDatepickerModule,MatNativeDateModule, MatSlideToggleModule,MatDialogModule, MatTabsModule,MatButtonToggleModule ]

})

export class MaterialModule {
}
 