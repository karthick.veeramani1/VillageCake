import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import{ AuthService } from '../../services/auth.service'
  import { from } from 'rxjs';
@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  form = new FormGroup({
    email: new FormControl('',Validators.required),
    phone: new FormControl(),
    password: new FormControl(),
    cPassword: new FormControl()
  })

  constructor( public authService : AuthService ) { }

  ngOnInit() {
  }
  log(){
    let email = this.form.get('email').value;
    let password = this.form.get('password').value;
    this.authService.createUser(email, password)
   
  }

  get email()
  {
    return this.form.get('email');
  }
}
