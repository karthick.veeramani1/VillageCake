import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit  { 
 isLogin = false;
 isSignup = false;
 isState = 0;
 fadeIn=false;
 fadeOut=false;
  constructor() { }
  loginToRegister()
{

  if (this.isState == 0 ){
    this.isSignup = !this.isSignup;
  }
  else{
    this.isSignup = !this.isSignup;
    this.isLogin = !this.isLogin;
  }  
  setTimeout(() => {
    
    if (this.isState==2 || this.isState==0)
    {
      this.isState=1;
    }
    else
    {
      this.isState=2;
    }
  }, 430);

}
  ngOnInit() {
   
  }

  fadeLoginClasses() {
    let classes =  {
    fadeOutSignin:this.isSignup,  
    fadeInSignin:this.isLogin    }
    return classes;

  }
  fadeSignupClasses() {
    let classes =  {
    fadeOutSignup:this.isLogin ,  
    fadeInSignup: this.isSignup  }
    return classes;

  }
  navClasses() {
    if (this.isLogin == true) {
      let classes =  {
        movingToLogin: this.isLogin          
    }
    return classes;
    } else (this.isSignup == true)
    {
      let classes =  {
        movingToSignup: this.isSignup 
    }
    return classes;
    }   
}

formClasses(){
  
  if (this.isLogin == true) {
    let classes =  {
      movingFormToLogin: this.isLogin          
  }
  return classes;
  } else (this.isSignup == true)
  {
    let classes =  {
      movingFormToSignup: this.isSignup 
  }
  return classes;
  }
}  

}
