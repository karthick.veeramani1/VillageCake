// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  hmr: false,
  GOOGLE_MAPS_API_KEY: 'AIzaSyDBQHqc9ddac8B-ZN4oEYEFSkO8mMmCLfI',
  firebase : {
    apiKey: "AIzaSyAlX1dROG2SkOH4qAnemhocdDuiAlLawuw",
    authDomain: "villagecake01.firebaseapp.com",
    databaseURL: "https://villagecake01.firebaseio.com",
    projectId: "villagecake01",
    storageBucket: "villagecake01.appspot.com",
    messagingSenderId: "705645137805"
  }
};
